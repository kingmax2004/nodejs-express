
const dotenv = require('dotenv');
dotenv.config({ path: './config/config.env' })
const fs = require('fs');

const app = require("./app");

const PORT = process.env.PORT || 3000;

//Start Server
const server = app.listen(PORT, () => {
    console.log(`App listening on ${process.env.NODE_ENV} port ${PORT}`);

    fs.appendFile('persistentDisk\message.txt', 'data to append\n', function (err) {
        if (err) throw err;
        console.log('persistentDisk available');
    });
});

process.on('unhandledRejection', (reason, promise) => {
    // unhandledRejections.set(promise, reason);
    //g(`Error: ${reason}`);
    server.close(() => process.exit(1))
});
