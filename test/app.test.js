const { describe } = require('mocha');
const { expect } = require('chai');
const request = require('supertest');
//const assert = require('assert');
const app = require('../app')

describe('Test API', function () {
    describe('Test Hello World API', function () {
        it('test 1', function (done) {
            request(app).get("/api/test")
                .end((err, res) => {
                    expect(res.statusCode).to.equal(200);
                    done();
                })
        });
        it('test 2', async () => {
            await request(app).get("/api/test")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);
                })
        });
        it('test 3', async () => {
            const response = await request(app).get("/api/test")
            expect(response.statusCode).to.equal(200);
        });
        it('should be Hello World text', async () => {
            await request(app).get('/api/test').expect(200)
                .then((res) => {
                    expect(res.text).to.equal('Hello, World');
                });
        });
    });
});