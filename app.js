const express = require('express');
const promBundle = require("express-prom-bundle");
const morgan = require('morgan')
const dotenv = require('dotenv');
dotenv.config({ path: './config/config.env' })
const fs = require('fs');

const common = require('./utilities/errorHandler')
const connectDB = require('./config/db')
const initialRouting = require('./endpoint.js');

const app = express();
const PORT = process.env.PORT || 3000;

//Connect DB
connectDB();

//body parser
app.use(express.json());

//Logging
// console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

//Prometheus
const metricsMiddleware = promBundle({
    includeMethod: true,
    includePath: true
});
app.use(metricsMiddleware);

// Routing
initialRouting(app);

//Middleware
app.use(common.errorHandler);

//Start Server
const server = app.listen(PORT, () => {
    console.log(`App listening on ${process.env.NODE_ENV} port ${PORT}`);

    fs.appendFile('persistentDisk\message.txt', 'data to append\n', function (err) {
        if (err) throw err;
        //console.log('persistentDisk available');
    });
});

process.on('unhandledRejection', (reason, promise) => {
    // unhandledRejections.set(promise, reason);
    //g(`Error: ${reason}`);
    server.close(() => process.exit(1))
});

module.exports = app;