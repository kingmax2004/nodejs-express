# Bootcamps
Using NodeJS v112, Express, Mocha, Chai, Mongo, Geocoder
### Develop Environment

- Install dependencies: npm install
- For test: npm test
- Hello world API: http://localhost:3000/api/test
- Get all bootcamps: http://localhost:3000/api/v1/bootcamps

## Run on Docker
### Run application
- docker run -d -p 3000:3000 --name bootcamps nqnguyen/bootcamps:1.4
### Prometheus + Grafana and EFK (Fluentbit) on Docker
- Prometheus + Grafana: CURRENT_UID=$(id -u):$(id -g) docker-compose up -d
- EFK: Docker-compose up -d
## Local Links
- Swagger-stats: http://{URL}:3000/swagger-stats/ux
- Swagger-stats/metric: http://{URL}:3000/swagger-stats/metrics
- Grafana: http://{URL}:3001
- Prometheus: http://{URL}:9090/
- Kibana: http://{URL}:5601/
- Elasticsearch: http://{URL}:9200/

Ref: 
- swagger-stats: https://github.com/slanatech/swagger-stats/

## CI/CD
### CI
- Build a private Gitlab Runner on EC2 with mode docker excutor 
- Build a docker image base on a dockerfile
- Push the image to Docker Hub

### CD
- Deploy on a EC2/GKE/EKS with mode NodePort/ LB/ Ingress
- Run the Deployment and Service yaml,...
