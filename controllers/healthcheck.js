const asyncHandler = require('../utilities/async');
const bootcampController = require('./bootcamps')


exports.liveness = asyncHandler(async (req, res, next) => {
    res.status(200).send({ success: true, message: 'App is running' })
})

exports.test = asyncHandler(async (req, res, next) => {
    res.status(200).send("Hello, World")
})


exports.readiness = bootcampController.getCountBootcamps