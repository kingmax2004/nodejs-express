const express = require('express');
const router = express.Router();

const { liveness, readiness,test } = require('../controllers/healthcheck')
/**
 * @swagger
 * /liveness:
 *  get:
 *      tags: 
 *      - "Health"
 *      description: check Liveness
 *      responses:
 *          '200': 
 *              description: successful get all bootcamps     
 */
router.route('/liveness')
    .get(liveness)

/**
* @swagger
* /readiness:
*  get:
*      tags: 
*      - "Health"
*      description: check readiness
*      responses:
*          '200': 
*              description: successful get all bootcamps     
*/
router.route('/readiness')
    .get(readiness)

router.route('/test')
    .get(test)
module.exports = router;