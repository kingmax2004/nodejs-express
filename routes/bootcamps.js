const express = require('express');
const router = express.Router();

const { getBootcamps, getBootcamp, createBootcamp, updateBootcamp, delBootcamp } = require('../controllers/bootcamps')

/**
 * @swagger
 * /v1/bootcamps:
 *  get:
 *      tags: 
 *      - "Bootcamps"
 *      description: get all bootcamps
 *      responses:
 *          '200': 
 *              description: successful get all bootcamps
 *  post:
 *      tags: 
 *      - "Bootcamps"
 *      description: create a new bootcamp
 *       
 */
router.route('/')
    .get(getBootcamps)
    .post(createBootcamp);

/**
 * @swagger
 * /v1/bootcamps/{id}:
 *      get:
 *          tags: 
 *          - "Bootcamps"
 *          description: get one bootcamps with ID
 *          parameters:
 *          - name: 'id'
 *            in: "path"
 *            description: "ID of bootcamp to return"
 *            required: true
 *            type: "string"
 *          responses:
 *              '200':
 *                  description: sucessfull get bootcamp with ID
 */
router.route('/:id')
    .get(getBootcamp)
    .put(updateBootcamp)
    .delete(delBootcamp)

module.exports = router;
